<?php
// Services Shortcode
function sprinklr_services_shortcode_function($atts, $content = null){
   extract(shortcode_atts(array(
      			'class'			=> '',
				'id'			=> '',
				'title'			=> '',
				'description'	=> '',
				'textcolor'		=> '#fff',
   ), $atts));

	$services_attr_id = '';
	if(!empty($id)){
		$services_attr_id='id="'.$id.'"';
	}
	$class_attr= '';
	if(!empty($class)){
		$class_attr='class="'.$class.'"';
	}
	$services_style = '';
	if(isset($textcolor) && !empty($textcolor)){
		$services_style .= 'color: '.$textcolor.';';
	}
	$services_style_attr='';
	if(!empty($heading_style)){
		$services_style_attr='style="'.$services_style.'"';
	}
	$return_string = '<div class="spr-services '.$class_attr.'" '.$services_attr_id.' '.$services_style_attr.' >';
   		$return_string .= '<div class="spr-top-image">
   			<img class="aligncenter size-full wp-image-9695" alt="plus" src="https://d3bg2441si4wp3.cloudfront.net/wp-content/uploads/2015/12/01202630/plus.png" width="51" height="51">
   		</div>';
   		$return_string = '<div class="spr-services-content">';
	   		if(isset($title) && !empty($title)){
	   			$return_string .= '<h2>'.$title.'</h2>';
	   		}
	   		if(isset($description) && !empty($description)){
	   			$return_string .= '<p>'.$description.'</p>';
	   		}
   			$return_string .= '<div class="spr-service-tabs">';
   			if(isset($content) && !empty($content)){
   				$return_string .= do_shortcode($content);
   			}
   			$return_string .= '</div>
   		</div>
    </div>';
   return $return_string;
}
// Heading Shortcode
function sprinklr_service_child_shortcode_function($atts, $content = null){
   extract(shortcode_atts(array(
      			'name'		=> '',
				'image'		=> '',
				'link'		=> '#',
				'target'	=> '',
   ), $atts));
   $service_item = '<div class="service-item">';
	   if(isset($image) && !empty($image)){
	   		$service_item .= '<figure><img src="'.$image.'"></figure>';
	   }
	   $service_item .= '<h4>'.$name.'</h4>';
	   $service_item .= '<p>'.$content.'</p>';
   $service_item .= '</div>';
   return $service_item;
}

add_shortcode('service', 'sprinklr_service_child_shortcode_function');
add_shortcode('services', 'sprinklr_services_shortcode_function');