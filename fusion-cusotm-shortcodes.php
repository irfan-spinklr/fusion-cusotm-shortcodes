<?php
/*
  Plugin Name: Presstigers Fusion core Customization
  Plugin URI: http://presstigers.com
  Description: PressTigers Fusion core Customization for shortcodes
  Version: 1.0
  Author: Muhammad Irfan - PressTigers
  Author URI: http://presstiger.com
  Text Domain: pt-custom-shortcodes
 */
function pt_custom_shortcodes_activate() {

    // Activation code here...
}
register_activation_hook( __FILE__, 'pt_custom_shortcodes_activate' );
//add_action( 'plugins_loaded', 'my_plugin_override' );


function pt_plugin_first() {
   
    // ensure path to this file is via main wp plugin path
    $wp_path_to_this_file = preg_replace('/(.*)plugins\/(.*)$/', WP_PLUGIN_DIR."/$2", __FILE__);
    $this_plugin = plugin_basename(trim($wp_path_to_this_file));
    $active_plugins = get_option('active_plugins');
    
    $this_plugin_key = array_search($this_plugin, $active_plugins);
    $current_item = $active_plugins[$this_plugin_key];
    unset($active_plugins[$this_plugin_key]);
    array_push($active_plugins, $current_item);
    update_option('active_plugins', $active_plugins);
    
}
add_action("plugins_loaded", "pt_plugin_first");

if( ! class_exists( 'custom_FusionCore_Plugin') && class_exists('FusionCore_Plugin') ) {
    class custom_FusionCore_Plugin extends FusionCore_Plugin {
        /**
         * Plugin version, used for cache-busting of style and script file references.
         *
         * @since   1.0.0
         *
         * @var  string
         */
        const VERSION = '1.7.4';
        
        /**
         * Instance of this class.
         *
         * @since   1.0.0
         *
         * @var   object
         */
        protected static $instance = null;
        
        /**
         * Initialize the plugin by setting localization and loading public scripts
         * and styles.
         *
         * @since    1.0.0
         */
          function __construct() {
            define('CUSTOM_SHORTCODES_URI', plugin_dir_url( __FILE__ ) . 'includes');
            define('CUSTOM_SHORTCODES_DIR', plugin_dir_path( __FILE__ ) .'includes');
            remove_action('wp_ajax_fusion_shortcodes_popup', array('FusionCore_Plugin', 'popup'), 10);
            add_action('wp_ajax_fusion_shortcodes_popup', array(&$this, 'popup'), 10);
        }
        
        /**
         * Popup function which will show shortcode options in thickbox.
         *
         * @return void
         */
        function popup() {


            require_once( CUSTOM_SHORTCODES_DIR . '/fusion-sc.php' );

            die();

        }
    }
    new custom_FusionCore_Plugin();
 }
require_once( 'shortcodes/hubspot.php' );
require_once( 'shortcodes/blog.php' );
require_once( 'shortcodes/services.php' );
require_once( 'shortcodes/class-heading.php' );


